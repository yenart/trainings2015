#include <iostream>

int
main()
{
    int counter = 1, number, largest;

    std::cout << "Enter the number: ";
    std::cin  >> number;

    largest = number;

    while (counter <= 10){
        if (number > largest){
            largest = number;
        }
        std::cout << "Enter the number: ";
        std::cin  >> number;
        
        counter++;

    }
    
    std::cout << "\nThe largest: " << largest << std::endl;
    
    return 0;
}
