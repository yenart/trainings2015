#include <iostream>
#include <iomanip>

int 
main()
{
    int time;
    double hourlySalary, salary;

    std::cout << "Enter working time (non-positive number to quit): ";
    std::cin  >>  time;

    while(time > 0){
        std::cout << "Enter hourly salary ($00.00): ";
        std::cin  >> hourlySalary;

        if (hourlySalary <= 0){
            std::cout << "Error 1: The hourly salary can't be non-positive.\nTry again." << std::endl;
            return 1;
        } else {
            if (time > 40){
                salary = (time * hourlySalary) + ((time - 40) * (hourlySalary / 2));
            } else {
                salary = time * hourlySalary;
            }
            std::cout << "Salary: " << std::setprecision(2) << std::fixed << salary;
        
            std::cout << "\n\nEnter working time (non-positive number to quit): ";
            std::cin  >>  time; 
        }
    }

    return 0;
}
