#include <iostream>

int
main ()
{
    int number, a, b, c, d, e;

    std::cout << "Enter five-digit number: ";
    std::cin >> number;
    
    
    if(number > 9999)
        if(number < 100000){	
            a = number / 10000;
            b = (number / 1000) % 10;
            c = (number / 100) % 10;
            d = (number / 10) % 10;
            e = number % 10;

            std::cout << a;
            std::cout << " " << b;
            std::cout << " " << c;
            std::cout << " " << d;
            std::cout << " " << e << std::endl;

            return 0;
        }
   
    std::cout << "Error\n" << std::endl;
    return 1;
}

