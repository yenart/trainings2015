#include <iostream>

int
main()
{
    int a,b;
    std::cout << "Enter two numbers: " << std::endl;
    std::cin >> a >> b;
    std::cout << "sum=" << a + b << std::endl;
    std::cout << "product=" << a * b << std::endl;
    std::cout << "difference=" << a - b << std::endl;
    std::cout << "quotient=" << a / b << std::endl;

    return 0;
}
