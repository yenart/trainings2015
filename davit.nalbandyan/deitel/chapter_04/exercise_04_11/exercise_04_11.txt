a) 
	if ( age >= 65 ) ; /// Wrong (I copyied and pasted)
		cout « "Возраст больше или равен 65" « endl; 
	else 
		cout « "Возраст менее 65 « endl"; 

a) 
	if ( age >= 65 ) { /// Right
		std::cout « "Возраст больше или равен 65" « std::endl;
	} 
	else { 
		std::cout « "Возраст менее 65" « std::endl;
	};
1)	First mistake was at line 2 ; symbol is suppose to be entirely in the end of
if operator.
2)	Second mistake if and else operators missing {} these brackets.
3)	Third mistake std:: was missing.
4)	Fourth mistake was at line 5 " symbol was suppose to be within cout and endl

b) if ( age >= 65 ) /// Wrong 
cout « "Возраст больше или равен 65" « endl; 
else 
cout « "Возраст менее 65" « endl; 

b) 
	if ( age >= 65 ) { /// Right
		std::cout « "Возраст больше или равен 65" « std::endl;
	} 
	else {
		std::cout « "Возраст менее 65" « std::endl; 
	}

c) 
	int x = 1, total; 
	while ( x <= 10 ) { 
	total += x; 
	++x; 
	}
1)in the beggining total must be 0 to add x

d) 
	while ( x <= 100 ) 
	total += x; 
	++x; 
1) {} brackets missing
2) x dont have a value
3) total dont have a value

e) 
	while ( у > 0 ) { 
	cout « у « endl; 
	++y; 
	}
1) std:: missing
2) y dont have a value
3) if the cycle will work it will not end and will cause multiple problems
