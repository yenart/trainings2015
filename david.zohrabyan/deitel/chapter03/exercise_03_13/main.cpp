/// Invoice class testing
#include <iostream>
#include "Invoice.hpp" /// Implementation of design’s Invoice

int
main()
{
    Invoice product1("Soap Powder", "PR-40532", 450, 10); /// creates the first object of a class Invoice and gives parameter
    Invoice product2("Hand Soap", "Pr-40533", 150, -60); /// creates the second object of a class Invoice and gives parameter 
    Invoice product3("Sponge", "Pr-40534", 200, 50); /// creates the third object of a class Invoice and gives parameter
    
    std::cout << "The list of products of market. " << std::endl;
    std::cout << "\tProduct name." << "\tArticle of product." << "\tQuantity." << "\tPrice." << std::endl;
    std::cout << "\t" << product1.getArticle() << "\t" << product1.getDescription() << "\t" << product1.getQuantity() << "\t" << product1.getPrice() << "\n";
    std::cout << "\t" << product2.getArticle() << "\t" << product2.getDescription() << "\t" << product2.getQuantity() << "\t" << product2.getPrice() << "\n";
    
    std::cout << "\t" << product3.getArticle() << "\t" << product3.getDescription() << "\t" << product3.getQuantity() << "\t" << product3.getPrice() << "\n";
    
    std::cout << "Total amount of " << product1.getArticle() << " is: " << product1.getInvoiceAmount() << std::endl;
    std::cout << "Total amount of " << product2.getArticle() << " is: " << product2.getInvoiceAmount() << std::endl;
    std::cout << "Total amount of " << product3.getArticle() << " is: " << product3.getInvoiceAmount() << std::endl;
    
    return 0;
}
    
