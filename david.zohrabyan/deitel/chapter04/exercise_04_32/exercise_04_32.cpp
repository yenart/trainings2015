#include <iostream>

int
main()
{
    std::cout << "The program determines and prints whether three floating-point values represent the sides of a triangle or not." << std::endl;    
    
    double a, b, c;
    std::cout << "Enter first length of a triangle (a > 0): " ;
    std::cin >> a;
    if(a <= 0) {
        std::cout << "Error 1. The length must be positive." << std::endl;
        return 1;
    }
    
    std::cout << "Enter second length of a triangle (b > 0): " ;
    std::cin >> b;
    if(b <= 0) {
        std::cout << "Error 1. The length must be positive." << std::endl;
        return 1;
    }
    
    std::cout << "Enter third length of a triangle (c > 0): " ;
    std::cin >> c;
    if(c <= 0) {
        std::cout << "Error 1. The length must be positive." << std::endl;
        return 1;
    }

        
    if(a >= b + c) {
        std::cout << "These parties can`t represent a triangle." << std::endl;
    } else if(b >= a + c) {
        std::cout << "These parties can`t represent a triangle." << std::endl;
    } else if(c >= a + b) {
        std::cout << "These parties can`t represent a triangle." << std::endl;
    } else {
        std::cout << "These parties can represent a triangle." << std::endl;
    }
        
    return 0;
}
