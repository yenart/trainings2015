#include <iostream>

int
main()
{
    int length;
    std::cout << "Enter the length(1-20): " ;
    std::cin >> length;
    if(length < 1) {
        std::cerr << "Error 1. The length should be in range [1, 20]" << std::endl;
        return 1;
    } else if(length > 20) {
        std::cerr << "Error 1. The length should be in range [1, 20]" << std::endl;
        return 1;   
    }
    
    int i = 0;
    while(i < length) {
        int j = 0;
        while(j < length) {
            if(0 == i) {          
                std::cout << "*";
            } else if(length - 1 == i) {
                std::cout << "*";
            } else if(0 == j) {
                std::cout << "*";  
            } else if(length - 1 == j) {
                std::cout << "*";
            } else {
                std::cout << " ";             
            }          
            ++j;
        }             
        std::cout << std::endl;
        ++i;
    }
    return 0;
}
