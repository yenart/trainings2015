#include <iostream>

int
main()
{
    double r, pi = 3.14159;
    
    std::cout << "Enter the radius of circle (r > 0): ";
    std::cin >> r;
    if(r <= 0) {
        std::cout << "Error 1. The radius must be positive." << std::endl;
        return 1;
    }
    
    std::cout << "d = " << 2 * r << std::endl;
    std::cout << "l = " << 2 * pi * r << std::endl;
    std::cout << "S = " << pi * r * r << std::endl;
    
    return 0;
}   
